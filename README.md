#Maven project configure with Nexus

intall java
$brew install openjdk@17

The command used to create the symbolic link is:
    sudo ln -sfn /usr/local/opt/openjdk@17/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

Open Intellij   
    java-maven-app : File->Project Structure now choose jdk 17

now we can start application successfully

build application through maven
    $brew install --ignore-dependencies maven
    
Configure pom.xml to connect to Nexus

cd ~
ls -a | grep .m2
ls .m2
cd .m
vim settings.xml
<settings>

    <servers>
        <server>
            <id>nexus-snapshot</id>
            <username>qqqq</username>
            <password>qqqq</password>
        </server>
    </servers>

</settings>  


java-maven-app$ mvn package
BUILD SUCCESS
check artifact:
java-maven-app sanvi$ ls target/


java-maven-app sanvi$ mvn deploy

